package br.ifmg.continuous.deployment.engenhariasoftware2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EngenhariaSoftware2Application {

    public static void main(String[] args) {
        SpringApplication.run(EngenhariaSoftware2Application.class, args);
    }

}
